<?php global $post, $options;
$pdflnk=get_post_meta( $post->ID, $options['prefix'].'attached_pdf', true );
$ref=get_post_meta( $post->ID, $options['prefix'].'reference', true );
?>
<div>
	<h5><?=$post->post_title?></h5>
	<?php
	remove_filter('get_the_excerpt', 'wp_trim_excerpt');
	add_filter('get_the_excerpt', 'new_wp_trim_excerpt'); ?>
	<p><?=get_the_excerpt()?></p>
	<?php
	remove_filter('get_the_excerpt', 'new_wp_trim_excerpt');
	add_filter('get_the_excerpt', 'wp_trim_excerpt'); ?>
	<?=(!empty($ref)) ? '<p class="pub-reference">'.$ref.'</p>' : null?>
	<a href="<?=$pdflnk?>" target="_blank" class="dat"><?=the_time('F d, Y')?></a>
	<br>
	<br>
	<a href="<?=$pdflnk?>" target="_blank" class="view"><?=__('View publication', 'occam')?></a>
</div>
