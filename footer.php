		</main>
		<footer class="footer">
			<div class="container">
				<div class="row">
					<?php generic_footer_nav(); ?>
					<div class="col-lg-2 col-md-2 col-xs-12 col-sm-12  copyright">
						<p> © Copyright <?php echo (date('Y')=='2017') ? date('Y') : date('2017-Y'); ?></p>
					</div>
				</div>
			</div>
		</footer>
		<?php wp_footer(); global $options; ?>
		<div class="md-modal md-effect-1 md-small modal-contact" id="modal-contact">
			<i class="fa fa-2x fa-times-circle-o colored md-close"></i>
			<div class="md-content">
				<div class="row">
					<div class="col-xs-12<?=(!empty($options['socl']['coord']))?' col-md-6':null?>">
						<h2><?=__('Contact us', 'occam')?></h2>
						<?php if(empty($options['socl']['fullinfo'])){ ?>
						<ul>
							<?php if(!empty($options['socl']['address'])){ ?>
							<li>
								<i class="fa fa-map-marker colored"></i> <?=$options['socl']['address']?>
							</li>
							<?php } ?>
							<?php if(!empty($options['socl']['website'])){ ?>
							<li>
								<i class="fa fa-globe colored"></i> <?=$options['socl']['website']?>
							</li>
							<?php } ?>
							<?php if(!empty($options['socl']['emails'])){ ?>
								<?php
								foreach (maybe_unserialize( $options['socl']['emails'] ) as $key => $val) { ?>
									<li>
										<i class="fa fa-envelope colored"></i> <?=$val?>
									</li>
								<?php }	?>
							<?php } ?>
							<?php if(!empty($options['socl']['phones'])){ ?>
								<?php
								foreach (maybe_unserialize( $options['socl']['phones'] ) as $key => $val) { ?>
									<li>
										<i class="fa fa-phone colored"></i> <?=$val?>
									</li>
								<?php }	?>
							<?php } ?>
							<?php if(!empty($options['socl']['work'])){ ?>
							<li>
								<i class="fa fa-clock-o colored"></i> <?=$options['socl']['work']?>
							</li>
							<?php } ?>
						</ul>
					<?php } else {
						echo $options['socl']['fullinfo'];
					} ?>
					</div>
					<?php if(!empty($options['socl']['coord'])){ ?>
					<div class="col-xs-12 col-md-6"><div id="map"></div></div>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="md-overlay"></div>
		<?php
		global $options;
		wp_footer();
		if(!empty($options['advd']['custom_css'])){
			echo '<style>'.htmlspecialchars_decode($options['advd']['custom_css']).'</style>';
		}
		if(!empty($options['advd']['custom_js'])){
			echo '<script>'.htmlspecialchars_decode($options['advd']['custom_js']).'</script>';
		}
		if(!empty($options['advd']['custom_js_tag'])){
			echo $options['advd']['custom_js_tag'];
		}
		if(!empty($options['advd']['custom_noscript'])){
			echo '<noscript>'.$options['advd']['custom_noscript'].'</noscript>';
		}
		?>
	</body>
</html>
