<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(); global $options, $post; ?></title>
		<link rel="apple-touch-icon" sizes="57x57" href="<?=get_template_directory_uri()?>/assets/favicon/apple-touch-icon-57x57.png?v=pgrzK">
		<link rel="apple-touch-icon" sizes="60x60" href="<?=get_template_directory_uri()?>/assets/favicon/apple-touch-icon-60x60.png?v=pgrzK">
		<link rel="apple-touch-icon" sizes="72x72" href="<?=get_template_directory_uri()?>/assets/favicon/apple-touch-icon-72x72.png?v=pgrzK">
		<link rel="apple-touch-icon" sizes="76x76" href="<?=get_template_directory_uri()?>/assets/favicon/apple-touch-icon-76x76.png?v=pgrzK">
		<link rel="apple-touch-icon" sizes="114x114" href="<?=get_template_directory_uri()?>/assets/favicon/apple-touch-icon-114x114.png?v=pgrzK">
		<link rel="apple-touch-icon" sizes="120x120" href="<?=get_template_directory_uri()?>/assets/favicon/apple-touch-icon-120x120.png?v=pgrzK">
		<link rel="apple-touch-icon" sizes="144x144" href="<?=get_template_directory_uri()?>/assets/favicon/apple-touch-icon-144x144.png?v=pgrzK">
		<link rel="apple-touch-icon" sizes="152x152" href="<?=get_template_directory_uri()?>/assets/favicon/apple-touch-icon-152x152.png?v=pgrzK">
		<link rel="apple-touch-icon" sizes="180x180" href="<?=get_template_directory_uri()?>/assets/favicon/apple-touch-icon-180x180.png?v=pgrzK">
		<link rel="icon" type="image/png" sizes="32x32" href="<?=get_template_directory_uri()?>/assets/favicon/favicon.png?v=pgrzK">
		<link rel="icon" type="image/png" sizes="194x194" href="<?=get_template_directory_uri()?>/assets/favicon/favicon-194x194.png?v=pgrzK">
		<link rel="icon" type="image/png" sizes="192x192" href="<?=get_template_directory_uri()?>/assets/favicon/android-chrome-192x192.png?v=pgrzK">
		<link rel="icon" type="image/png" sizes="16x16" href="<?=get_template_directory_uri()?>/assets/favicon/favicon.png?v=pgrzK">
		<link rel="manifest" href="<?=get_template_directory_uri()?>/assets/favicon/manifest.json?v=pgrzK">
		<link rel="mask-icon" href="<?=get_template_directory_uri()?>/assets/favicon/safari-pinned-tab.svg?v=pgrzK" color="#082B3E">
		<link rel="shortcut icon" href="<?=get_template_directory_uri()?>/assets/favicon/favicon.ico?v=pgrzK">
		<meta name="msapplication-TileColor" content="#082b3e">
		<meta name="msapplication-TileImage" content="<?=get_template_directory_uri()?>/assets/favicon/mstile-144x144.png?v=pgrzK">
		<meta name="msapplication-config" content="<?=get_template_directory_uri()?>/assets/favicon/browserconfig.xml?v=pgrzK">
		<meta name="theme-color" content="#082b3e">
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?>" href="<?php bloginfo('rss2_url'); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class('notloaded'); ?>>
		<div id="preloader" class="preloader-wrap">
			<div class="spinner">
			  <div class="dot1"></div>
			  <div class="dot2"></div>
			</div>
		</div>
		<header>
			<nav class="navbar navbar-default" role="navigation">
				<div class="container">
					<div class="row">
						<div class="container-fluid">
							<div class="navbar-left col-lg-3 col-md-3 col-sm-4 col-xs-9">
								<?php
								$logo=(empty($options['gnrl']['header_logo_id'])) ? '<img src="'.$options['tpldir'].'/assets/img/logo.png" alt="logo" class="header__center-logo-img">' : remove_width_attribute(wp_get_attachment_image( $options['gnrl']['header_logo_id'], 'full', false, array('class'=>'logo img-responsive') ));
								if(!is_front_page()){ ?>
									<?php if(function_exists('icl_get_languages')){
										$home_url = apply_filters( 'wpml_home_url', get_option( 'home' ) );
									} else {
										$home_url='/';
									} ?>
									<a href="<?php echo $home_url; ?>">
										<?= $logo ?>
									</a>
								<?php } else { ?>
									<?= $logo ?>
								<?php } ?>
							</div>
							<div class="navbar-header col-xs-12 ">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
							<div class="collapse navbar-collapse col-lg-8 col-md-7 col-xs-6 col-sm-7" id="bs-example-navbar-collapse-1">
								<?php generic_nav(); ?>
							</div>
							<div class="navbar-right col-lg-1 col-md-1 col-sm-2 col-xs-12">
								<?php if(!empty($options['socl']['facebook'])) { ?>
								<a href="<?= $options['socl']['facebook'] ?>" target="_blank"> <i class="fa fa-facebook" aria-hidden="true"></i></a>
								<?php } ?>
                                <?php if(!empty($options['socl']['vk'])) { ?>
								<a href="<?= $options['socl']['vk'] ?>" target="_blank"> <i class="fa fa-vk" aria-hidden="true"></i></a>
                                <?php } ?>
                                <?php if(!empty($options['socl']['twitter'])) { ?>
								<a href="<?= $options['socl']['twitter'] ?>" target="_blank"> <i class="fa fa-twitter" aria-hidden="true"></i></a>
                                <?php }
								if(function_exists('icl_get_languages')){
									$langs=icl_get_languages('skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str');
									if(count($langs)>0){
										$i=0;
										$curlang=$languages='';
										foreach ($langs as $l) {
											$languages.='<a href="'.((!$l['active']) ? $l['url'] : '#').'" '.(($l['active']) ? 'class="lang active"' : 'class="lang"').'>'.mb_substr($l['translated_name'], 0, 2).'</a>';
										}
										echo $languages;
									} else {
										echo '<a href="#" class="lang"> En </a><a href="#" class="lang"> Ru </a>';
									}
								} else { ?>
								<a href="#" class="lang"> En </a>
								<a href="#" class="lang"> Ru </a>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</nav>
			<?php if(!empty($options['pmeta']['_wp_page_template'][0])&&$options['pmeta']['_wp_page_template'][0]=='template-main.php'){ ?>
			<div class="block">
				<div class="container">
					<div class="row">
						<div class="headlin col-lg-6">
							<?php
							// print_r($options['pmeta']);
							$htext=(!empty($options['pmeta'][$options['prefix'].'head-text'][0]))?$options['pmeta'][$options['prefix'].'head-text'][0]:null;
							$hlink=(!empty($options['pmeta'][$options['prefix'].'head-link'][0]))?$options['pmeta'][$options['prefix'].'head-link'][0]:'#';
							$hlink1=(!empty($options['pmeta'][$options['prefix'].'head-link1'][0]))?$options['pmeta'][$options['prefix'].'head-link1'][0]:'#';
							if(!empty($htext)){ echo $htext; } ?>
						</div>
					</div>
					<div class="btn btn1">
						<span><a class="learn_a" href="<?=$hlink?>"><?=__( 'How we plan to do it', 'occam' )?></a></span>
						<div class="dot"></div>
					</div>
					<div class="btn btn2">
						<span><a class="learn_a" href="<?=$hlink1?>"><?=__( 'Why we do it', 'occam' )?></a></span>
						<div class="dot"></div>
					</div>
				</div>
			</div>
			<?php } ?>
		</header>
		<main>
