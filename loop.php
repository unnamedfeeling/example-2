<?php
global $options;
$lnk=(!empty(get_post_meta( $post->ID, $options['prefix'].'post_link', true ))) ? get_post_meta( $post->ID, $options['prefix'].'post_link', true ) : get_the_permalink();
 ?>
	<div class="blog_item row">
		<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 img_wrap">
			<?php if(!has_post_thumbnail( $post->ID )){ ?>
			<img src="<?=get_template_directory_uri()?>/assets/img/img_blog1.jpg" alt="">
			<?php } else {
				echo wp_get_attachment_image( get_post_thumbnail_id( $post->ID ), 'loop-post', false, array('alt'=>$post->post_title) );
			} ?>
		</div>
		<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
			<h3><?=$post->post_title?></h3>
			<?php
			remove_filter('get_the_excerpt', 'wp_trim_excerpt');
			add_filter('get_the_excerpt', 'new_wp_trim_excerpt'); ?>
			<p><?=get_the_excerpt()?></p>
			<?php
			remove_filter('get_the_excerpt', 'new_wp_trim_excerpt');
			add_filter('get_the_excerpt', 'wp_trim_excerpt'); ?>
			<div class="blog_bottom">
				<a class="blog_a" href="<?=$lnk?>"><?=__('Read More', 'occam')?></a>
				<a href="<?=$lnk?>" class="dat"><?=the_time('F d, Y')?></a>
				<div class="clear"></div>
			</div>
		</div>
	</div>
