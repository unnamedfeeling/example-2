<?php
function generic_metaboxes() {
	// Start with an underscore to hide fields from custom fields list
	global $options;
	// $prefix = 'startime_';
	$prefix = 'occam_';
	// print_r($prefix);

	/**
	 * Initiate the metabox
	 */
	if(isset($_GET['post'])){
		$post_id = $_GET['post'];
	} elseif(isset($_POST['post_ID'])){
		$post_id = $_POST['post_ID'];
	} else {
		$post_id='';
	}
	$mpost=get_post($post_id);
	// print_r($post_id);
	//$post_id = get_the_ID() ;
	$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
	// $pt=$mpost->post_type;
	// $pt=get_post_type($post_id);
	// print_r($pt);
	// $pt=get_current_screen()->post_type;
	// check for a template type
	$cmb_publications = new_cmb2_box( array(
		'id'            => 'publications_metabox',
		'title'         => __( 'This post type data', 'cmb2' ),
		'object_types'  => array( 'publications', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );
	$cmb_publications->add_field( array(
		'name'    => __( 'Publication reference', 'cmb2' ),
		'id'      => $prefix.'reference',
		'type'    => 'textarea_small'
	) );
	$cmb_publications->add_field( array(
		'name'    => __( 'Attached PDF', 'cmb2' ),
		'id'      => $prefix.'attached_pdf',
		'type'    => 'file',
		'query_args' => array(
			'type' => array('application/pdf')
		)
	) );
	$cmb_posts = new_cmb2_box( array(
		'id'            => 'posts_metabox',
		'title'         => __( 'This post type data', 'cmb2' ),
		'object_types'  => array( 'post', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );
	$cmb_posts->add_field( array(
		'name'    => __( 'External post link', 'cmb2' ),
		'id'      => $prefix.'post_link',
		'type'    => 'text'
	) );
	if($template_file=='template-main.php'){
		$cmb = new_cmb2_box( array(
			'id'            => 'mainpage_metabox',
			'title'         => __( 'Данные для этой страницы', 'cmb2' ),
			'object_types'  => array( 'page', ), // Post type
			'context'       => 'normal',
			'priority'      => 'high',
			'show_names'    => true, // Show field names on the left
			// 'cmb_styles' => false, // false to disable the CMB stylesheet
			// 'closed'     => true, // Keep the metabox closed by default
		) );
		$cmb->add_field( array(
			'name'    => __( 'Header text', 'cmb2' ),
			'id'      => $prefix.'head-text',
			'type'    => 'wysiwyg'
		) );
		$cmb->add_field( array(
			'name'    => __( 'Header link', 'cmb2' ),
			'id'      => $prefix.'head-link',
			'type'    => 'text'
		) );
		$cmb->add_field( array(
			'name'    => __( 'Header link 2', 'cmb2' ),
			'id'      => $prefix.'head-link1',
			'type'    => 'text'
		) );
		$cmb->add_field( array(
			'name'    => __( 'About text and button', 'cmb2' ),
			'id'      => $prefix.'about-text',
			'type'    => 'wysiwyg'
		) );
		$cmb->add_field( array(
			'name'    => __( 'Research text', 'cmb2' ),
			'id'      => $prefix.'research-text',
			'type'    => 'wysiwyg'
		) );
		$research_group = $cmb->add_field( array(
			'id'          => $prefix.'projects',
			'type'        => 'group',
			// 'description' => __( 'Профкурсы', 'startimecamp' ),
			// 'repeatable'  => false, // use false if you want non-repeatable group
			'options'     => array(
				'group_title'   => __( 'Element {#}', 'occam' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add', 'occam' ),
				'remove_button' => __( 'Remove', 'occam' ),
				'sortable'      => true, // beta
				// 'closed'     => true, // true to have the groups closed by default
			),
		) );
		// Id's for group's fields only need to be unique for the group. Prefix is not needed.
		$cmb->add_group_field( $research_group, array(
			'name' => __( 'Project name', 'occam' ),
			'id'   => 'title',
			'type' => 'text',
			// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		) );
		$cmb->add_group_field( $research_group, array(
			'name' => __( 'Project description (repeatable)', 'occam' ),
			'id'   => 'description',
			'type' => 'text',
			'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		) );
		$cmb->add_group_field( $research_group, array(
			'name' => __( 'Project description', 'occam' ),
			'id'   => 'descr',
			'type' => 'wysiwyg',
			// 'type' => 'text',
			// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		) );
		$cmb->add_field( array(
			'name'    => __( 'Publications description text', 'cmb2' ),
			'id'      => $prefix.'publications-descr',
			'type'    => 'wysiwyg'
		) );
		$cmb->add_field( array(
			'name'    => __( 'Attached publications', 'cmb2' ),
			// 'desc'    => __( 'Указать товары, которые будут отображаться на данной странице', 'cmb2' ),
			'id'      => $prefix.'attached_publications',
			'type'    => 'custom_attached_posts',
			'options' => array(
				'show_thumbnails' => false, // Show thumbnails on the left
				'filter_boxes'    => false, // Show a text box for filtering the results
				'query_args'      => array(
					'posts_per_page' => 999,
					'post_type'      => 'publications',
				)
			),
		) );
		$cmb->add_field( array(
			'name'    => __( 'Blog description text', 'cmb2' ),
			'id'      => $prefix.'blog-descr',
			'type'    => 'wysiwyg'
		) );
		$cmb->add_field( array(
			'name'    => __( 'Attached post', 'cmb2' ),
			// 'desc'    => __( 'Указать товары, которые будут отображаться на данной странице', 'cmb2' ),
			'id'      => $prefix.'attached_post',
			'type'    => 'custom_attached_posts',
			'options' => array(
				'show_thumbnails' => false, // Show thumbnails on the left
				'filter_boxes'    => false, // Show a text box for filtering the results
				'query_args'      => array(
					'posts_per_page' => 999,
					'post_type'      => 'post',
				)
			),
		) );
		$cmb->add_field( array(
			'name'    => __( 'Hiring description text', 'cmb2' ),
			'id'      => $prefix.'hiring-descr',
			'type'    => 'wysiwyg'
		) );
		$hiring_group = $cmb->add_field( array(
			'id'          => $prefix.'hiring',
			'type'        => 'group',
			// 'description' => __( 'Профкурсы', 'startimecamp' ),
			// 'repeatable'  => false, // use false if you want non-repeatable group
			'options'     => array(
				'group_title'   => __( 'Element {#}', 'occam' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => __( 'Add', 'occam' ),
				'remove_button' => __( 'Remove', 'occam' ),
				'sortable'      => true, // beta
				// 'closed'     => true, // true to have the groups closed by default
			),
		) );
		// Id's for group's fields only need to be unique for the group. Prefix is not needed.
		$cmb->add_group_field( $hiring_group, array(
			'name' => __( 'Vacancy title', 'occam' ),
			'id'   => 'title',
			'type' => 'text',
			// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		) );
		$cmb->add_group_field( $hiring_group, array(
			'name' => __( 'Vacancy description', 'occam' ),
			'id'   => 'description',
			'type' => 'text',
			'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		) );
		$cmb->add_field( array(
			'name'    => __( 'Contact form shortcode', 'cmb2' ),
			'desc'	  => __('please add \'html_class="form-inline"\' in the end of the shortcode for it to show properly styled form', 'occam'),
			'id'      => $prefix.'contact-form',
			'type'    => 'text'
		) );
	}

}
