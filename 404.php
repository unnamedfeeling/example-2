<?php get_header(); ?>
	<main role="main" aria-label="Content">
		<section>
			<article id="post-404">
				<div class="container">
					<div class="row">
						<h1><?php _e( 'Page not found', 'html5blank' ); ?></h1>
						<h2>
							<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'html5blank' ); ?></a>
						</h2>
					</div>
				</div>
			</article>
		</section>
	</main>
<?php get_footer(); ?>
