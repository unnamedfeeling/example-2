<?php /* Template Name: Main Page Template */ get_header(); global $options; ?>
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<section id="about" class="about ">
				<div class="container  helpful4">
					<div class="row">
						<h2><?=__('About us', 'occam')?></h2>
						<?php
						$about_text=(!empty($options['pmeta'][$options['prefix'].'about-text'][0]))?$options['pmeta'][$options['prefix'].'about-text'][0]:null;
						echo $about_text;
						?>
					</div>
				</div>
			</section>
			<section id="research" class="research">
				<div class="block"></div>
				<div class="container">
					<div class="row">
						<h2 class=" helpful4"><?=__('Research', 'occam')?></h2>
						<div class="col-lg-6"></div>
						<div class="col-lg-6">
							<?php
							$research_text=(!empty($options['pmeta'][$options['prefix'].'research-text'][0]))?$options['pmeta'][$options['prefix'].'research-text'][0]:null;
							echo $research_text;
							?>
							<div class="projects helpful4">
								<h3 class=" helpful4"><?=__('Research projects', 'occam')?></h3>
								<div class="open_tabs row">
									<?php
									// $projects=(!empty($options['pmeta'][$options['prefix'].'projects'][0]))?maybe_unserialize($options['pmeta'][$options['prefix'].'projects'][0]):null;
									// if(!empty($projects)){
									// 	$html1=$html2='';
									// 	$projects1 = array_slice($projects, 0, count($projects) / 2, true);
									// 	$projects2 = array_slice($projects, count($projects) / 2, 999, true);
									// 	$i=1;
									// 	foreach ($projects1 as $prj) {
									// 		// print_r($prj);
									// 		$list='';
									// 		if(count($prj['description'])>1){
									// 			foreach ($prj['description'] as $v) {
									// 				$list.='<p>'.$v.'</p>';
									// 			}
									// 		} else {
									// 			$list=$prj['description'][0];
									// 		}
									// 		$html1.='<li><a class="show-block" href="#block'.$i.'">'.$prj['title'].'</a><div id="block'.$i.'">'.$list.'</div></li>';
									// 		$i++;
									// 		unset($list);
									// 	}
									// 	// $i=1;
									// 	foreach ($projects2 as $prj) {
									// 		// print_r($prj);
									// 		$list='';
									// 		if(count($prj['description'])>1){
									// 			foreach ($prj['description'] as $v) {
									// 				$list.='<p>'.$v.'</p>';
									// 			}
									// 		} else {
									// 			$list=$prj['description'][0];
									// 		}
									// 		$html2.='<li><a class="show-block" href="#block'.$i.'">'.$prj['title'].'</a><div id="block'.$i.'">'.$list.'</div></li>';
									// 		$i++;
									// 		unset($list);
									// 	}
									// 	unset($list, $html);
									// }
									$projects=(!empty($options['pmeta'][$options['prefix'].'projects'][0]))?maybe_unserialize($options['pmeta'][$options['prefix'].'projects'][0]):null;
									if(!empty($projects)){
										// $html1=$html2='';
										$html='';
										$i=1;
										foreach ($projects as $prj) {
											// print_r($prj);
											$list='';
											// if(count($prj['description'])>1){
											// 	foreach ($prj['description'] as $v) {
											// 		$list.='<p>'.$v.'</p>';
											// 	}
											// } else {
											// 	$list=$prj['description'][0];
											// }
											$list=(!empty($prj['descr']))?$prj['descr']:null;
											$html.='<li><a class="show-block" href="#block'.$i.'">'.$prj['title'].'</a><div id="block'.$i.'">'.$list.'</div></li>';
											$i++;
											unset($list);
										}
									}
									?>
									<ul class="col-xs-12">
										<?=$html?>
									</ul>
									<?php
									// <ul class="col-lg-6 col-md-6 col-sm-6">
									// 	$html2
									// </ul>
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section id="publications" class="publications">
				<h2><?=__('Publications', 'occam')?></h2>
				<div class="container">
					<?php
					$publications_text=(!empty($options['pmeta'][$options['prefix'].'publications-descr'][0]))?$options['pmeta'][$options['prefix'].'publications-descr'][0]:null;
					echo $publications_text;
					?>
					<div id="content_8" class="publ_item content2">
						<section class="regular slider">
							<?php
							$args=array(
								'post_type'=>'publications',
								'posts_per_page'=>-1
							);
							if(!empty($options['pmeta'][$options['prefix'].'attached_publications'][0])){
								$args['post__in']=maybe_unserialize( $options['pmeta'][$options['prefix'].'attached_publications'][0] );
								$args['orderby']='post__in';
							}
							$publications=new WP_Query($args);
							if ($publications->have_posts()): while ($publications->have_posts()) : $publications->the_post();
									get_template_part( 'loop', 'publications' );
							endwhile;
							else: ?>
								<article>
									<h2><?= __( 'Sorry, nothing to display.', 'occam' ); ?></h2>
								</article>
							<?php endif;
							wp_reset_query();
							?>
						</section>
					</div>
				</div>
			</section>
			<section id="blog" class="blog">
				<div class="container">
					<h2><?=__('Blog', 'occam')?></h2>
					<?php
					$blog_text=(!empty($options['pmeta'][$options['prefix'].'blog-descr'][0]))?$options['pmeta'][$options['prefix'].'blog-descr'][0]:null;
					echo $blog_text;
					?>
					<div id="content_1" class="content">
						<?php
						$args=array(
							'post_type'=>'post',
							'posts_per_page'=>-1
						);
						if(!empty($options['pmeta'][$options['prefix'].'attached_publications'][0])){
							$args['post__in']=maybe_unserialize( $options['pmeta'][$options['prefix'].'attached_publications'][0] );
							$args['orderby']='post__in';
						}
						$publications=new WP_Query($args);
						if ($publications->have_posts()): while ($publications->have_posts()) : $publications->the_post();
							get_template_part( 'loop' );
						endwhile;
						else: ?>
							<article>
								<h2><?= __( 'Sorry, nothing to display.', 'occam' ); ?></h2>
							</article>
						<?php endif;
						wp_reset_query();
						?>
					</div>
				</div>
			</section>
			<section id="hiring" class="hiring">
				<div class="container">
					<h2><?=__('Hiring', 'occam')?></h2>
					<?php
					$hiring_text=(!empty($options['pmeta'][$options['prefix'].'hiring-descr'][0]))?$options['pmeta'][$options['prefix'].'hiring-descr'][0]:null;
					echo $hiring_text;
					?>
					<div class="form row">
						<div class="col-lg-6 col-md-6 col-xs-12 col-sm-6">
							<h3><?=__('Current jobs:', 'occam')?></h3>
							<?php
							$vacancies=(!empty($options['pmeta'][$options['prefix'].'hiring'][0])) ? maybe_unserialize( $options['pmeta'][$options['prefix'].'hiring'][0] ) : null;
							if(!empty($vacancies)){
								$html='';
								foreach ($vacancies as $vac) {
									$list='';
									if(count($vac['description'])>1){
										foreach ($vac['description'] as $v) {
											$list.='<li>'.$v.'</li>';
										}
									} else {
										$list='<li>'.$vac['description'][0].'</li>';
									}
									$html.='<h6>'.$vac['title'].'</h6><ul>'.$list.'</ul>';
								}
								echo $html;
							}
							?>
						</div>
						<div class="col-lg-6 col-md-6 col-xs-12 col-sm-6 ">
							<div>
								<?php
								$form=(!empty($options['pmeta'][$options['prefix'].'contact-form'][0])) ? $options['pmeta'][$options['prefix'].'contact-form'][0] : '[contact-form-7 title="Main page contact form" html_class="form-inline"]';
								echo do_shortcode($form); ?>
							</div>
						</div>
					</div>
				</div>
			</section>
			<style>
				.about:before{
					content: '<?=__('About', 'occam')?>';
				}
				.research:before{
					content: '<?=__('Research', 'occam')?>';
				}
				.publications:before{
					content: '<?=__('Publications', 'occam')?>';
				}
				.blog:before{
					content: '<?=__('Blog', 'occam')?>';
				}
				/*.hiring:before{
					content: '<?=__('Hiring', 'occam')?>';
				}*/
			</style>
		<?php endwhile; ?>
		<?php else: ?>
			<article>
				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
			</article>
		<?php endif; ?>

<?php get_footer(); ?>
