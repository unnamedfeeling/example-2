<?php get_header(); ?>
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<div class="nav_wrap">
			<div class="container">
				<a class="back" href="<?=get_home_url()?>"><i class="fa fa-home" aria-hidden="true"></i><?=__('back home', 'occam')?></a>
			</div>
		</div>
		<div class="wrapper">
			<div class="container">
				<div class="col-xs-12"><h1><?=$post->post_title?></h1></div>
				<div class="col-xs-12 cont-data">
					<?php the_content(); ?>
				</div>
				<div class="col-xs-12">
					<?=edit_post_link()?>
				</div>
			</div>
		</div>

	<?php endwhile; ?>
	<?php else: ?>
		<article>
			<h1><?php _e( 'Sorry, nothing to display.', 'occam' ); ?></h1>
		</article>
	<?php endif; ?>
<?php get_footer(); ?>
